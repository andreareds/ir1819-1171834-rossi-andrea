clc;
clear all;
close all;
NUMBER_OF_TOPICS = 50;

AP_BM25SWPS = zeros(NUMBER_OF_TOPICS,1);
Prec_BM25SWPS = zeros(NUMBER_OF_TOPICS,1);
P10_BM25SWPS = zeros(NUMBER_OF_TOPICS,1);
iprec_recall_BM25SWPS = zeros(11,1);

AP_BM25PS = zeros(NUMBER_OF_TOPICS,1);
Prec_BM25PS = zeros(NUMBER_OF_TOPICS,1);
P10_BM25PS = zeros(NUMBER_OF_TOPICS,1);
iprec_recall_BM25PS = zeros(11,1);

AP_TF_IDFSWPS = zeros(NUMBER_OF_TOPICS,1);
Prec_TF_IDFSWPS = zeros(NUMBER_OF_TOPICS,1);
P10_TF_IDFSWPS = zeros(NUMBER_OF_TOPICS,1);
iprec_recall_TF_IDFSWPS = zeros(11,1);

AP_TF_IDF = zeros(NUMBER_OF_TOPICS,1);
Prec_TF_IDF = zeros(NUMBER_OF_TOPICS,1);
P10_TF_IDF = zeros(NUMBER_OF_TOPICS,1);
iprec_recall_TF_IDF = zeros(11,1);

BM25SWPS = fopen('BM25_SW_PS.txt','r');
topicId=1;
interIndex = 1;
while ~feof(BM25SWPS)
    line = strsplit(fgetl(BM25SWPS));
%     if(strcmp(line(2),'all')==1)
%         disp(line(1));
%     end
%     if(length(line(1))>16)
%         disp(extractBetween(line(1),1,16));
%     end
    if(strcmp(line(1),'map')==1 && strcmp(line(2),'all')~=1)
        AP_BM25SWPS(topicId) = str2double(line(3));
    elseif(strcmp(line(1) ,'Rprec')==1&& strcmp(line(2),'all')~=1)
        Prec_BM25SWPS(topicId) = str2double(line(3));
    elseif(cellfun('length',line(1))>16 && strcmp(extractBetween(line(1),1,16),'iprec_at_recall_')==1 && strcmp(line(2),'all')==1)
        iprec_recall_BM25SWPS(interIndex) = str2double(line(3));
        interIndex = interIndex+1;
    elseif(strcmp(line(1),'P_10')==1&& strcmp(line(2),'all')~=1)
        P10_BM25SWPS(topicId)=str2double(line(3));
        topicId = topicId+1;
    end
end

% for i=1:NUMBER_OF_TOPICS
%     disp(AP_TFIDFSWPS(i));
% end

BM25PS = fopen('BM25_PS.txt','r');
topicId=1;
interIndex = 1;
while ~feof(BM25PS)
    line = strsplit(fgetl(BM25PS));
    if(strcmp(line(1),'map')==1&& strcmp(line(2),'all')~=1)
        AP_BM25PS(topicId) = str2double(line(3));
    elseif(strcmp(line(1) ,'Rprec')==1&& strcmp(line(2),'all')~=1)
        Prec_BM25PS(topicId) = str2double(line(3));
    elseif(cellfun('length',line(1))>16 && strcmp(extractBetween(line(1),1,16),'iprec_at_recall_')==1 && strcmp(line(2),'all')==1)
        iprec_recall_BM25PS(interIndex) = str2double(line(3));
        interIndex = interIndex+1;
    elseif(strcmp(line(1),'P_10')==1&& strcmp(line(2),'all')~=1)
        P10_BM25PS(topicId)=str2double(line(3));
        topicId = topicId+1;
    end
end

TFIDFSWPS = fopen('TF_IDF_SW_PS.txt','r');
topicId=1;
interIndex = 1;
while ~feof(TFIDFSWPS)
    line = strsplit(fgetl(TFIDFSWPS));
    if(strcmp(line(1),'map')==1&& strcmp(line(2),'all')~=1)
        AP_TF_IDFSWPS(topicId) = str2double(line(3));
    elseif(strcmp(line(1) ,'Rprec')==1&& strcmp(line(2),'all')~=1)
        Prec_TF_IDFSWPS(topicId) = str2double(line(3));
    elseif(cellfun('length',line(1))>16 && strcmp(extractBetween(line(1),1,16),'iprec_at_recall_')==1 && strcmp(line(2),'all')==1)
        iprec_recall_TF_IDFSWPS(interIndex) = str2double(line(3));
        interIndex=interIndex+1;
    elseif(strcmp(line(1),'P_10')==1&& strcmp(line(2),'all')~=1)
        P10_TF_IDFSWPS(topicId)=str2double(line(3));
        topicId = topicId+1;
        %disp(str2double(line(3)));
    end
end

TFIDF = fopen('TF_IDF.txt','r');
topicId = 1;
interIndex=1;
while ~feof(TFIDF)
    line = strsplit(fgetl(TFIDF));
    if(strcmp(line(1),'map')==1&& strcmp(line(2),'all')~=1)
        AP_TF_IDF(topicId) = str2double(line(3));
    elseif(strcmp(line(1) ,'Rprec')==1&& strcmp(line(2),'all')~=1)
        Prec_TF_IDF(topicId) = str2double(line(3));
    elseif(cellfun('length',line(1))>16 && strcmp(extractBetween(line(1),1,16),'iprec_at_recall_')==1 && strcmp(line(2),'all')==1)
        iprec_recall_TF_IDF(interIndex) = str2double(line(3));
        interIndex = interIndex+1;
    elseif(strcmp(line(1),'P_10')==1&& strcmp(line(2),'all')~=1)
        P10_TF_IDF(topicId)=str2double(line(3));
        topicId = topicId+1;
    end
end
disp(AP_BM25PS);
runID = {'BM25 SW PS', 'BM25 PS', 'TF IDF SW PS', 'TF IDF'};
%disp(runID);
topicID = 351:400;
%disp(topicID);
recall = 0.00:0.1:1.00;
measure = cat(2,AP_BM25SWPS,AP_BM25PS, AP_TF_IDFSWPS, AP_TF_IDF);
save ap_HW.mat measure runID topicID
disp(measure);
disp('\n\nFINE\n\n');
measure = cat(2,iprec_recall_BM25SWPS,iprec_recall_BM25PS,iprec_recall_TF_IDFSWPS,iprec_recall_TF_IDF);
save iprec_at_recall_HW.mat measure runID recall
measure = cat(2,Prec_BM25SWPS,Prec_BM25PS,Prec_TF_IDFSWPS,Prec_TF_IDF);
save rprec_HW.mat measure runID topicID
disp(measure);
disp('\n\nFINE\n\n');
measure = cat(2,P10_BM25SWPS, P10_BM25PS, P10_TF_IDFSWPS,P10_TF_IDF);
save p10_HW.mat measure runID topicID
disp(measure)